package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.backgroundService;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;

public class NextPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background_service_next_page);
    }
}
