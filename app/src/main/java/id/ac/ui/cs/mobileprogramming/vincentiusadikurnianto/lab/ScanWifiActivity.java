package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab;
//tesg
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.constant.EndPoint;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.custom_adapter.CustomWifiAdapter;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.helper.FrequencyConverter;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.lab6Util.Lab6CppUtil;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.model.WifiApModel;

import java.util.ArrayList;
import java.util.List;


public class ScanWifiActivity extends AppCompatActivity {
    private WifiManager wifiMngr;
    private List<WifiApModel> wifiApModels = new ArrayList<>();
    private boolean asyncTaskIsCompleted = false;
    private WifiScanReceiver wifiReceiver;
    private CustomWifiAdapter networkAdapter;
    private boolean isScanning = false;
    private boolean iEnabledWifi = false;
    private final int MY_PERMISSION_COARSE_LOCATION = 0;
    private boolean permissions_all_good = true;
    final String TAG = "SWA";

    public WifiApModel createDummyWifiAp(){
        String ssid = "Dummy";
        String bssid = "99:99:99:99:99:99";
        int frequency = 99;
        int channel = 98;
        int signal = 97;
        String security = "[Nothing]";
        return new Lab6CppUtil().createWifiFromCpp3(ssid,bssid,security);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi__scan_);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        wifiMngr = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        networkAdapter = new CustomWifiAdapter();
        wifiReceiver = new WifiScanReceiver();

        ListView listV = (ListView)findViewById(R.id.listView);
        listV.setAdapter(networkAdapter);

        Button button = (Button) findViewById(R.id.post_results_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!asyncTaskIsCompleted){
                    Toast toast = Toast.makeText(ScanWifiActivity.this,"Async Task Wifi Scan has not been completed",Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();

                }else{


                    String toastMessage = "None";

                    if (wifiApModels.size()>=1){

                        RequestQueue requestQueue = Volley.newRequestQueue(ScanWifiActivity.this);
                        final JSONObject jsonObject = new JSONObject();

                        try{

                            JSONArray jsonArray = new JSONArray();

                            for (WifiApModel wifiApModel: wifiApModels){
                                Log.d(TAG,"wifi: "+wifiApModel.getSsid());
                                JSONObject wifi = new JSONObject();
                                wifi.put("ssid",wifiApModel.getSsid());
                                wifi.put("bssid",wifiApModel.getBssid());
                                wifi.put("frequency",wifiApModel.getFrequency());
                                wifi.put("channel",wifiApModel.getChannel());
                                wifi.put("signal",wifiApModel.getSignal());
                                wifi.put("security",wifiApModel.getSecurity());
                                jsonArray.put(wifi);

                            }
                            jsonObject.put("access points",jsonArray);


                        } catch ( JSONException e){
                            e.printStackTrace();
                        }

                        String endpoint = EndPoint.ENDPOINT;
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, endpoint, jsonObject,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d(TAG, "Success Posting Data");
                                        Log.d(TAG,"payload: "+jsonObject.toString());

                                        Toast toast = Toast.makeText(ScanWifiActivity.this, "Success Posting Data", Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.TOP, 0, 0);
                                        toast.show();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e(TAG, "Error Getting Response");
                                Log.e(TAG,"Response: "+String.valueOf(error.networkResponse));
                                Toast toast = Toast.makeText(ScanWifiActivity.this, "Error Getting Response", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.TOP, 0, 0);
                                toast.show();
                            }
                        });
                        requestQueue.add(jsonObjectRequest);



                    }else{
                        toastMessage="Async Task Wifi Scan has been completed, but no access point detected";
                        Toast toast = Toast.makeText(ScanWifiActivity.this,toastMessage,Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP, 0, 0);
                        toast.show();
                    }

                }
            }
        });

        listV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView myTextView0 = (TextView) view.findViewById(R.id.net_ssid);
                TextView myTextView1 = (TextView) view.findViewById(R.id.net_bssid);
                String text0 = myTextView0.getText().toString();
                String text1 = myTextView1.getText().toString();
                // send some data bundled with the intent
                Intent intent = new Intent(ScanWifiActivity.this, LocateWifiActivity.class);
                intent.putExtra("data0", text0);
                intent.putExtra("data1", text1);
                startActivity(intent);
            }
        });

        startWifiScan();
        Log.d(TAG,"num of wifi: "+wifiReceiver.wifiApModelsss.size());

    }

    // menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // menu clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                //showSettings();
                Intent intent0 = new Intent(this, SettingsActivity.class);
                startActivity(intent0);
                return true;
            case R.id.action_exit:
                SharedPreferences sharedPref = getSharedPreferences("myPref", MODE_PRIVATE);
                boolean iEnabledWifi = sharedPref.getBoolean("iEnabledWifi", false);
                boolean iEnabledBt = sharedPref.getBoolean("iEnabledBt", false);

                if (iEnabledWifi) {
                    WifiManager wifiM = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifiM.setWifiEnabled(false);
                    sharedPref.edit().putBoolean("iEnabledWifi", false).apply();
                }

//                if (iEnabledBt) {
//                    BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
//                    btAdapter.disable();
//                    sharedPref.edit().putBoolean("iEnabledBt", false).apply();
//                }

                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"masuk on pause");
        stopWifiScan();
        unregisterReceiver(wifiReceiver);



        if (iEnabledWifi) {
            SharedPreferences.Editor editor = getSharedPreferences("myPref", MODE_PRIVATE).edit();
            editor.putBoolean("iEnabledWifi", true).apply();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"masuk on resume");
        registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        startWifiScan();
    }

    // create a receiver
    private class WifiScanReceiver extends BroadcastReceiver{

        private List<WifiApModel> wifiApModelsss = new ArrayList<>();


        public void onReceive(Context c, Intent intent) {
            String band = PreferenceManager.getDefaultSharedPreferences(ScanWifiActivity.this).getString("wifi_band", "2400");
            List<ScanResult> wifiScanList = wifiMngr.getScanResults();
            List<WifiApModel> apList = new ArrayList<>();
            for (ScanResult result : wifiScanList) {
                switch (band) {
                    case "2400":
                        if (result.frequency < 2500) {
                            WifiApModel network = new WifiApModel(result.SSID, result.BSSID,
                                    result.frequency, FrequencyConverter.convert(result.frequency), result.level,
                                    result.capabilities);
                            apList.add(network);
                        }
                        break;
                    case "5000":
                        if (result.frequency > 5000) {
                            WifiApModel network = new WifiApModel(result.SSID, result.BSSID,
                                    result.frequency, FrequencyConverter.convert(result.frequency), result.level,
                                    result.capabilities);
                            apList.add(network);
                        }
                        break;
                }
            }

            if (permissions_all_good) {
//                WifiApModel dummy  = new Lab6CppUtil().createWifiFromCpp2();
                WifiApModel dummy  = createDummyWifiAp();
                Log.d(TAG,"dummy ssid: "+dummy.getSsid());
                apList.add(dummy);

                networkAdapter.setNetworkList(apList);
                Log.d(TAG,"Wifi Scan Receiver, permission_all_good");
                wifiApModels = apList;
                asyncTaskIsCompleted = true;
                Log.d(TAG,"num of wifi right now: "+wifiApModels.size());


            }
        }
    }

    public class WifiScanAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            int interval = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(ScanWifiActivity.this)
                    .getString("wifi_interval", "1000"));
            while (isScanning) {
                wifiMngr.startScan();

                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


    }



    public void startWifiScan() {

        isScanning = true;
        // enable wifi if is off
        if (!wifiMngr.isWifiEnabled()) {
            wifiMngr.setWifiEnabled(true);
            Toast.makeText(getApplicationContext(), R.string.wifi_is_enabled, Toast.LENGTH_SHORT).show();
            iEnabledWifi = true;
        }

        // check API level and permissions, and request permissions if not granted
        if((Build.VERSION.SDK_INT >= 23) && (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)) {
            permissions_all_good = false;
            requestPermissions();
        } else {
            // Start AsyncTask to scan for networks in the background
            new WifiScanAsyncTask().execute();
        }
    }

    public void stopWifiScan() {
        isScanning = false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSION_COARSE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // task you need to do.
                    // Start AsyncTask to scan for networks in the background
                    permissions_all_good =true;
                    new WifiScanAsyncTask().execute();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getApplicationContext(), "ACCESS_COARSE_LOCATION permission is not granted. Abort.", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}