package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.connectivityTest;

public interface NetworkListener {

    public void updateNetworkStatus(int result);
}