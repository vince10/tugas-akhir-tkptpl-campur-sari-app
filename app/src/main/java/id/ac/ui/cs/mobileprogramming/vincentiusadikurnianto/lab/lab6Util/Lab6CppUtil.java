package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.lab6Util;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.model.WifiApModel;

public class Lab6CppUtil {

    static {
        System.loadLibrary("native-lib");
    }

    public native WifiApModel createWifiFromCpp(String ssid, String bssid, String security, int frequency, int channel, int signal);
    public native WifiApModel createWifiFromCpp2();
    public native WifiApModel createWifiFromCpp3(String ssid, String bssid,String security);
}
