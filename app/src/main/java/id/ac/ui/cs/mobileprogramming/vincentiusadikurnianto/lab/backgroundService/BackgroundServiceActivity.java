package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.backgroundService;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;

public class BackgroundServiceActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonStart, buttonStop,buttonNext;
    String TAG = "MY SERVEICE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backgroun_service_fragment);

        buttonStart = findViewById(R.id.buttonStart);
        buttonStop = findViewById(R.id.buttonStop);
        buttonNext =  findViewById(R.id.buttonNext);


        buttonStart.setOnClickListener(this);
        buttonStop.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View src) {
        Log.d(TAG,src.getId()+"");

        switch (src.getId()) {

            case 2131296354:
                startService(new Intent(this, SongService.class));
                break;
            case 2131296355:
                stopService(new Intent(this, SongService.class));
                break;
            case 2131296352:
                Intent intent=new Intent(this,NextPage.class);
                Log.d(TAG,src.getId()+"     aaaa");
                startActivity(intent);
                Log.d(TAG,src.getId()+"   aaaa");
                break;
        }
    }
}

