package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.openGL;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;


public class OpenGLActivity extends AppCompatActivity {

    CubeGLTextureView mGLTextureView;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (detectOpenGLES30()) {
            //so we know it a opengl 3.0 and use our extended GLTextureView
            mGLTextureView = new CubeGLTextureView(this);
            mGLTextureView.setRenderer(new CubeRenderer(this));
            setContentView(mGLTextureView);

        } else {
            // This is where you could create an OpenGL ES 2.0 and/or 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2.
            Log.e("openglcube", "OpenGL ES 3.0 not supported on device.  Exiting...");
            finish();

        }

    }

    private boolean detectOpenGLES30() {
        ActivityManager am =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        return (info.reqGlEsVersion >= 0x30000);
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop the animation.
        mGLTextureView.setPaused(true);

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        if (mGLTextureView.isRunning == false) {
            //everything is closed down and we have problem.  Restart
            mGLTextureView = new CubeGLTextureView(this);
            mGLTextureView.setRenderer(new CubeRenderer(this));
            setContentView(mGLTextureView);
        }
        //start up the animation.
        mGLTextureView.setPaused(false);

    }
}
