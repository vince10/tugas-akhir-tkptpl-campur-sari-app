package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.custom_adapter.CustomWifiAdapter;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.helper.FrequencyConverter;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.helper.SignalLevel;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.model.WifiApModel;

public class LocateWifiFragment extends Fragment {
    private WifiManager wifiMngr;
    private LocateWifiFragment.WifiScanReceiver wifiReceiver;
    private CustomWifiAdapter networkAdapter;
    private String ap_ssid;
    private String ap_bssid;
    private boolean isScanning = false;
    private boolean sound = false;
    private Button soundButton;
    private MediaPlayer mp;
    private int signalLevel;
    private int newSignalLevel;
    final String TAG = "LWAFF";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.activity_wifi_locate_,container,false);
//       getActivity().setContentView(view);
       requireActivity().setContentView(R.layout.activity_wifi_locate_);

        Log.d(TAG,"start on create view locate wifi");
       getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        wifiMngr = (WifiManager)getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        networkAdapter = new CustomWifiAdapter();
        wifiReceiver = new LocateWifiFragment.WifiScanReceiver();
        ListView listV = (ListView)view.findViewById(R.id.listView2);
        listV.setAdapter(networkAdapter);

        Intent intent = getActivity().getIntent();
        Bundle bndl = intent.getExtras();
        ap_ssid = bndl.getString("data0");
        ap_bssid = bndl.getString("data1");

        // button to start/stop sound
        soundButton = (Button) view.findViewById(R.id.soundBtn);
        soundButton.setOnClickListener(buttonListener);

        startWifiScan();

        Log.d(TAG,"finish on create view locate wifi");

        return view;

    }

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        getActivity().setContentView(R.layout.activity_wifi_locate_);
//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//        wifiMngr = (WifiManager)getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
//        networkAdapter = new CustomWifiAdapter();
//        wifiReceiver = new LocateWifiFragment.WifiScanReceiver();
//
//        ListView listV = (ListView)getActivity().getView(R.id.listView2);
//        listV.setAdapter(networkAdapter);
//
//        // extract data from bundle
//        Intent intent = getActivity().getIntent();
//        Bundle bndl = intent.getExtras();
//        ap_ssid = bndl.getString("data0");
//        ap_bssid = bndl.getString("data1");
//
//        // button to start/stop sound
//        soundButton = (Button) this.findViewById(R.id.soundBtn);
//        soundButton.setOnClickListener(buttonListener);
//
//        startWifiScan();
//    }
//
    private View.OnClickListener buttonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d(TAG,"button is pressed");
            if (!sound) {
                sound = true;
                soundButton.setText(R.string.sound_off);
                signalLevel =newSignalLevel;
                soundController(1);
            } else {
                sound = false;
                soundButton.setText(R.string.sound_on);
                soundController(2);
            }
        }
    };

    public void soundController(int param) {

        switch (param) {
            case 0:
                if (newSignalLevel != signalLevel && sound) {
                    signalLevel = newSignalLevel;
                    Pause();
                    setMediaPlayer();
                    Play();
                }
                break;
            // value -1 is set by Broadcast Receiver when AP is out of range
            case 1:
                if (mp == null && signalLevel != -1) {
                    setMediaPlayer();
                    Play();
                }
                break;
            case 2:
                Pause();
                break;
            case 3:
                signalLevel = newSignalLevel;
                Pause();
                break;
        }
    }

    public void setMediaPlayer () {

        switch (newSignalLevel) {
            case 0:
                mp = MediaPlayer.create(getContext(), R.raw.b4);
                break;
            case 1:
                mp = MediaPlayer.create(getContext(), R.raw.b3);
                break;
            case 2:

                mp = MediaPlayer.create(getContext(), R.raw.b2);
                break;
            case 3:
                mp = MediaPlayer.create(getContext(), R.raw.b1);
                break;
            case 4:
                mp = MediaPlayer.create(getContext(), R.raw.b0);
                break;
        }
    }

    public void Play() {
        mp.setLooping(true);
        mp.start();
    }

    public void Pause()  {
        if (mp != null) {
            mp.release();
            mp = null;
        }
    }

    // menu

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    // menu clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                //showSettings();
                Intent intent0 = new Intent(getContext(), SettingsActivity.class);
                startActivity(intent0);
                return true;
            case R.id.action_exit:
                SharedPreferences sharedPref = getActivity().getSharedPreferences("myPref", getActivity().MODE_PRIVATE);
                boolean iEnabledWifi = sharedPref.getBoolean("iEnabledWifi", false);
                boolean iEnabledBt = sharedPref.getBoolean("iEnabledBt", false);

                if (iEnabledWifi) {
                    WifiManager wifiM = (WifiManager)getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifiM.setWifiEnabled(false);
                    sharedPref.edit().putBoolean("iEnabledWifi", false).apply();
                }

//                if (iEnabledBt) {
//                    BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
//                    btAdapter.disable();
//                    sharedPref.edit().putBoolean("iEnabledBt", false).apply();
//                }

                getActivity().finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopWifiScan();
        getActivity().unregisterReceiver(wifiReceiver);
        if (mp != null) {
            Pause();
        }
        sound = false;
        soundButton.setText(R.string.sound_on);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        startWifiScan();
    }

    // create a receiver
    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            List<ScanResult> wifiScanList = wifiMngr.getScanResults();
            List<WifiApModel> apList = new ArrayList<>();
            boolean foundAp = false;
            for (ScanResult result : wifiScanList) {
                if (result.BSSID.equals(ap_bssid)) {
                    foundAp = true;
                    WifiApModel network = new WifiApModel(result.SSID, result.BSSID,
                            result.frequency, FrequencyConverter.convert(result.frequency), result.level,
                            result.capabilities);
                    apList.add(network);
                    newSignalLevel = SignalLevel.determineLevel(result.level);
                    soundController(0);
                }
            }
            if (!foundAp) {
                WifiApModel network = new WifiApModel(ap_ssid, ap_bssid, 0, 0, -100, "");
                apList.add(network);
                newSignalLevel = -1;
                soundController(3);
            }
            // set the list you want to be printed
            networkAdapter.setIsSelected();
            networkAdapter.setNetworkList(apList);
            Log.d(TAG,"Wifi Scan Receiver, onReceive");
            Log.d(TAG,apList.toString());
        }
    }

    public class WifiScanAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            while (isScanning) {
                int interval = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext())
                        .getString("wifi_interval", "1000"));
                wifiMngr.startScan();
                try {
                    Thread.sleep(interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }

    public void startWifiScan() {
        isScanning = true;
        // Start AsyncTask to scan for networks in the background
        new LocateWifiFragment.WifiScanAsyncTask().execute();
    }

    public void stopWifiScan() {
        isScanning = false;
    }


}
