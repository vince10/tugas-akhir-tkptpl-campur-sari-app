package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.backgroundService;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;

public class SongService extends Service {
    MediaPlayer myPlayer;
    String TAG = "MY SERVEICE";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        Log.d(TAG,"before songg");
        myPlayer = MediaPlayer.create(this, R.raw.ehrlingdancewithme);
        Log.d(TAG,"after songg");
        myPlayer.setLooping(false); // Set looping
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        myPlayer.start();
        return START_NOT_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        myPlayer.start();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        myPlayer.stop();
    }
}