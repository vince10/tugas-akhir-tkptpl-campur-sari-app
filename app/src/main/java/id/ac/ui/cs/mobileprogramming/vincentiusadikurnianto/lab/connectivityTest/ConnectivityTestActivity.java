package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.connectivityTest;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;

public class ConnectivityTestActivity extends AppCompatActivity implements NetworkListener {

    private BroadcastReceiver MyReceiver = null;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setContentView(R.layout.connectivity_test_fragment);
        MyReceiver = new NetworkReceiver();
        textView = (TextView) findViewById(R.id.connection);

        broadcastIntent();
    }

    public void broadcastIntent() {
        registerReceiver(MyReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void updateTheTextView(int status) {
        switch (status){
            case 0:
                textView.setText("Connection: "+"No Internet");
                break;
            case 1:
                textView.setText("Connection: "+"Wifi");
                break;
            case 2:
                textView.setText("Connection: "+"Mobile");
                break;
            default:
                textView.setText("Connection: "+"Unknown");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(MyReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(MyReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void updateNetworkStatus(int result) {
        updateTheTextView(result);
    }
}
