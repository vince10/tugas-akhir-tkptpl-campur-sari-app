package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.connectivityTest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NetworkReceiver extends BroadcastReceiver {

    private NetworkListener listener;
    String TAG ="NRNRNR";

    @Override
    public void onReceive(Context context, Intent intent) {


        listener = (NetworkListener)context;
        int status = NetworkUtil.getConnectivityStatusString(context);
        if(status == 0) {
            status= 0;
        }
        listener.updateNetworkStatus(status);
    }
}