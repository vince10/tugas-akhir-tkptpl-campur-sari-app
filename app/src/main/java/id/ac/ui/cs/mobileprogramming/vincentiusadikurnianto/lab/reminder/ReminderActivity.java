package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.reminder;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.reminder.Adapter.EventAdapter;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.reminder.Database.DatabaseClass;
import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.reminder.Database.EntityClass;

public class ReminderActivity  extends AppCompatActivity implements View.OnClickListener {
    Button createEvent;

    EventAdapter eventAdapter;
    RecyclerView recyclerview;
    DatabaseClass databaseClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reminder_fragment);
        createEvent = (Button) findViewById(R.id.btn_createEvent);
        recyclerview = findViewById(R.id.recyclerview);
        createEvent.setOnClickListener(this);
        databaseClass = DatabaseClass.getDatabase(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAdapter();

    }

    private void setAdapter() {
        List<EntityClass> classList = databaseClass.EventDao().getAllData();
        eventAdapter = new EventAdapter(getApplicationContext(), classList);
        recyclerview.setAdapter(eventAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view == createEvent) {
            goToCreateEventActivity();
        }
    }

    private void goToCreateEventActivity() {
        Intent intent = new Intent(getApplicationContext(), CreateEvent.class);
        startActivity(intent);
    }
}

