package id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.connectivityTest;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import id.ac.ui.cs.mobileprogramming.vincentiusadikurnianto.lab.R;

public class ConnectivityTestFragment extends Fragment implements NetworkListener {
    private BroadcastReceiver MyReceiver = null;
    private TextView textView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connectivity_test_fragment,container,false);
        MyReceiver = new NetworkReceiver();
        textView = (TextView) getView().findViewById(R.id.connection);

        broadcastIntent();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(MyReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(MyReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void broadcastIntent() {
        getActivity().registerReceiver(MyReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public void updateTheTextView(int status) {
        switch (status){
            case 0:
                textView.setText(R.string.is_connected+": "+R.string.no_internet);
                break;
            case 1:
                textView.setText(R.string.is_connected+": "+R.string.wifi);
                break;
            case 2:
                textView.setText(R.string.is_connected+": "+R.string.wifi);
                break;
            default:
                textView.setText(R.string.is_connected+": "+R.string.unknown);
        }
    }


    @Override
    public void updateNetworkStatus(int result) {
        updateTheTextView(result);
    }
}