//
// Created by khiyara99 on 02/12/20.
//

#include "CppSource.h"
#include <jni.h>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

static const JNINativeMethod gMethods[] = {
        {"createWifiFromCpp3",
         "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lid/ac/ui/cs/mobileprogramming/vincentiusadikurnianto/lab/model/WifiApModel;",
         (void *) jniCreateWifiFromCpp3}
};

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved) {

    JNIEnv *env = NULL;
    jint result = -1;

    if (vm->GetEnv((void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        return result;
    }
    jclass classs = env->FindClass("id/ac/ui/cs/mobileprogramming/vincentiusadikurnianto/lab/lab6Util/Lab6CppUtil");
    if (classs == NULL) {
        return result;
    }

    jint count = sizeof(gMethods) / sizeof(gMethods[0]);

    if (env->RegisterNatives(classs, gMethods, count) != JNI_OK) {
        return result;
    }

    result = JNI_VERSION_1_6;


    return result;
}

#ifdef __cplusplus
}
#endif
