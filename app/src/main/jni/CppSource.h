#ifndef LAB_CPPSOURCE_H
#define LAB_CPPSOURCE_H

#include <jni.h>

typedef struct{
    char ssid[64];
    char bssid[64];
    char security[64];
    int frequency;
    int channel;
    int signal;


}
WifiApModel;

jobject jniCreateWifiFromCpp(JNIEnv *env, jobject, jstring ssid, jstring bssid, jstring security,int frequency, int channel, int signal);
jobject jniCreateWifiFromCpp2(JNIEnv *env, jobject);

jobject jniCreateWifiFromCpp3(JNIEnv *env, jobject, jstring ssid,jstring bssid, jstring security);

#endif //CPPSOURCE_H