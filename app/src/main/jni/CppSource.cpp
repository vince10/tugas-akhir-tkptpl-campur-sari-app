#include <string>
#include "CppSource.h"

jobject jniCreateWifiFromCpp(JNIEnv *env, jobject, jstring ssid, jstring bssid, jstring security, int frequency, int channel, int signal){
    jobject wifiObject = NULL;
    jclass wifiClass = env->FindClass("id/ac/ui/cs/mobileprogramming/vincentiusadikurnianto/lab/model/WifiApModel");

    jfieldID ssidField = env->GetFieldID(wifiClass, "ssid", "Ljava/lang/String;");
    jfieldID bssidField  = env->GetFieldID(wifiClass, "bssid", "Ljava/lang/String;");
    jfieldID frequencyField   = env->GetFieldID(wifiClass, "frequency", "I");
    jfieldID channelField   = env->GetFieldID(wifiClass, "channel", "I");
    jfieldID signalField   = env->GetFieldID(wifiClass, "signal", "I");
    jfieldID securityField  = env->GetFieldID(wifiClass, "security", "Ljava/lang/String;");


    wifiObject = env->AllocObject(wifiClass);
    env->SetObjectField(wifiObject,ssidField, (jstring)ssid);
    env->SetObjectField(wifiObject,bssidField, (jstring)bssid);
    env->SetIntField(wifiObject,frequencyField, (jint) frequency);
    env->SetIntField(wifiObject,channelField, (jint) channel);
    env->SetIntField(wifiObject,signalField, (jint) signal);
    env->SetObjectField(wifiObject,securityField, (jstring)security);



    return wifiObject;
}


jobject jniCreateWifiFromCpp2(JNIEnv *env, jobject){
    jobject wifiObject = NULL;
    jclass wifiClass = env->FindClass("id/ac/ui/cs/mobileprogramming/vincentiusadikurnianto/lab/model/WifiApModel");

    jfieldID ssidField = env->GetFieldID(wifiClass, "ssid", "Ljava/lang/String;");
    jfieldID bssidField  = env->GetFieldID(wifiClass, "bssid", "Ljava/lang/String;");
    jfieldID frequencyField   = env->GetFieldID(wifiClass, "frequency", "I");
    jfieldID channelField   = env->GetFieldID(wifiClass, "channel", "I");
    jfieldID signalField   = env->GetFieldID(wifiClass, "signal", "I");
    jfieldID securityField  = env->GetFieldID(wifiClass, "security", "Ljava/lang/String;");

    std::string ssidString = "Dummy";
    jstring ssidJS = env->NewStringUTF(ssidString.c_str());
    std::string bssidString = "99:99:99:99:99:99";
    jstring bssidJS = env->NewStringUTF(bssidString.c_str());
    std::string securityString = "None";
    jstring securityJS = env->NewStringUTF(securityString.c_str());

    wifiObject = env->AllocObject(wifiClass);
    env->SetObjectField(wifiObject,ssidField, (ssidJS));
    env->SetObjectField(wifiObject,bssidField, bssidJS);
    env->SetIntField(wifiObject,frequencyField, (jint) 100);
    env->SetIntField(wifiObject,channelField, (jint) 101);
    env->SetIntField(wifiObject,signalField, (jint) -1);
    env->SetObjectField(wifiObject,securityField, securityJS);



    return wifiObject;
}

jobject jniCreateWifiFromCpp3(JNIEnv *env, jobject, jstring ssid,jstring bssid,jstring security){
    jobject wifiObject = NULL;
    jclass wifiClass = env->FindClass("id/ac/ui/cs/mobileprogramming/vincentiusadikurnianto/lab/model/WifiApModel");

    jfieldID ssidField = env->GetFieldID(wifiClass, "ssid", "Ljava/lang/String;");
    jfieldID bssidField  = env->GetFieldID(wifiClass, "bssid", "Ljava/lang/String;");
    jfieldID frequencyField   = env->GetFieldID(wifiClass, "frequency", "I");
    jfieldID channelField   = env->GetFieldID(wifiClass, "channel", "I");
    jfieldID signalField   = env->GetFieldID(wifiClass, "signal", "I");
    jfieldID securityField  = env->GetFieldID(wifiClass, "security", "Ljava/lang/String;");

    const char *wifiSSID = env ->GetStringUTFChars(ssid,NULL);
    std::string ssidString = wifiSSID;
    jstring ssidJS = env->NewStringUTF(ssidString.c_str());
    const char *wifiBSSID = env ->GetStringUTFChars(bssid,NULL);
    std::string bssidString = wifiBSSID;
    jstring bssidJS = env->NewStringUTF(bssidString.c_str());
    const char *wifiSecurity= env ->GetStringUTFChars(security,NULL);
    std::string securityString = wifiSecurity;
    jstring securityJS = env->NewStringUTF(securityString.c_str());

    wifiObject = env->AllocObject(wifiClass);
    env->SetObjectField(wifiObject,ssidField, (ssidJS));
    env->SetObjectField(wifiObject,bssidField, bssidJS);
    env->SetIntField(wifiObject,frequencyField, (jint) 100);
    env->SetIntField(wifiObject,channelField, (jint) 101);
    env->SetIntField(wifiObject,signalField, (jint) -100);
    env->SetObjectField(wifiObject,securityField, securityJS);



    return wifiObject;
}

